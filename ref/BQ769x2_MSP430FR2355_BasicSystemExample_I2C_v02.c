//******************************************************************************
//   MSP430FR2355 example code for BQ769x2 
//
//   Rev 00 - 2/12/2020 Initial version. Cycles through examples from SLUAA11 app note
//   Rev 01 - 2/18/2020 Added CRC_Mode. Now I2C and I2C+CRC mode can be selected by changing CRC_Mode
//						Checks CRC from BQ769x2 on I2C Reads to make sure it is valid.
//   Rev 02 - 2/25/2020 Builds on Rev 01, but sets up basic example code for a real battery system
//
//
//   I2C Functions come from MSP430FR231x Demo - eUSCI_B0, I2C Master multiple byte TX/RX
//
//   Description: I2C master communicates to BQ769x2 sending and receiving
//   different command types. I2C master will enter LPM0 mode
//   while waiting for the messages to be sent/receiving using I2C interrupt.
//   ACLK = NA, MCLK = SMCLK = DCO 16MHz.
//
//                                     /|\ /|\
//                   MSP430FR2355     4.7k |
//                 -----------------    |  4.7k
//            /|\ |             P1.3|---+---|-- I2C Clock (UCB0SCL)
//             |  |                 |       |
//             ---|RST          P1.2|-------+-- I2C Data (UCB0SDA)
//                |                 |
//                |             P1.4|---- BQ76942 Alert Pin (interrupt)
//                |                 |
//          SW2---|P2.3         P6.6|--- Green LED
//                |                 |
//          SW1---|P4.1         P1.0|--- Red LED
//                |                 |
//
//******************************************************************************

#include <msp430.h> 
#include <stdint.h>
#include <stdbool.h>

#define SLAVE_ADDR  0x08  // BQ769x2 address is 0x10 including R/W bit or 0x8 as 7-bit address
#define CRC_Mode 0  // 0 for disabled, 1 for enabled


/* CMD_TYPE_X_SLAVE are example commands the master sends to the slave.
 * The slave will send example SlaveTypeX buffers in response.
 *
 * CMD_TYPE_X_MASTER are example commands the master sends to the slave.
 * The slave will initialize itself to receive MasterTypeX example buffers.
 * */


#define MAX_BUFFER_SIZE     20

/* TX_*Byte are example buffers initialized in the master, they will be
 * sent by the master to the slave.
 * RX_*Byte are example buffers initialized in the slave, they will be
 * sent by the slave to the master.
 * */
// Create Buffers for 2, 3, or 4 bytes of data
uint8_t TX_2Byte [2] = {0x00, 0x00};
uint8_t TX_3Byte [3] = {0x00, 0x00, 0x00};
uint8_t TX_4Byte [4] = {0x00, 0x00, 0x00, 0x00};
uint8_t TX_Buffer [MAX_BUFFER_SIZE] = {0};

uint8_t RX_2Byte [2] = {0x00, 0x00};
uint8_t RX_3Byte [3] = {0x00, 0x00, 0x00};
uint8_t RX_4Byte [4] = {0x00, 0x00, 0x00, 0x00};
uint8_t RX_Buffer [MAX_BUFFER_SIZE] = {0};


//******************************************************************************
// General I2C State Machine ***************************************************
//******************************************************************************

typedef enum I2C_ModeEnum{
    IDLE_MODE,
    NACK_MODE,
    TX_REG_ADDRESS_MODE,
    RX_REG_ADDRESS_MODE,
    TX_DATA_MODE,
    RX_DATA_MODE,
    SWITCH_TO_RX_MODE,
    SWITHC_TO_TX_MODE,
    TIMEOUT_MODE
} I2C_Mode;


/* Used to track the state of the software state machine*/
I2C_Mode MasterMode = IDLE_MODE;

/* The Register Address/Command to use*/
uint8_t TransmitRegAddr = 0;

/* ReceiveBuffer: Buffer used to receive data in the ISR
 * RXByteCtr: Number of bytes left to receive
 * ReceiveIndex: The index of the next byte to be received in ReceiveBuffer
 * TransmitBuffer: Buffer used to transmit data in the ISR
 * TXByteCtr: Number of bytes left to transfer
 * TransmitIndex: The index of the next byte to be transmitted in TransmitBuffer
 * */
uint8_t ReceiveBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t RXByteCtr = 0;
uint8_t ReceiveIndex = 0;
uint8_t TransmitBuffer[MAX_BUFFER_SIZE] = {0};
uint8_t TXByteCtr = 0;
uint8_t TransmitIndex = 0;


/* I2C Write and Read Functions */

/* For slave device with dev_addr, writes the data specified in *reg_data
 *
 * dev_addr: The slave device address.
 *           Example: SLAVE_ADDR
 * reg_addr: The register or command to send to the slave.
 *           Example: CMD_TYPE_0_MASTER
 * *reg_data: The buffer to write
 *           Example: MasterType0
 * count: The length of *reg_data
 *           Example: TYPE_0_LENGTH
 *  */
I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count);

/* For slave device with dev_addr, read the data specified in slaves reg_addr.
 * The received data is available in ReceiveBuffer
 *
 * dev_addr: The slave device address.
 *           Example: SLAVE_ADDR
 * reg_addr: The register or command to send to the slave.
 *           Example: CMD_TYPE_0_SLAVE
 * count: The length of data to read
 *           Example: TYPE_0_LENGTH
 *  */
I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count);
void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count);


I2C_Mode I2C_Master_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count)
{
    /* Initialize state machine */
    MasterMode = TX_REG_ADDRESS_MODE;
    TransmitRegAddr = reg_addr;
    RXByteCtr = count;
    TXByteCtr = 0;
    ReceiveIndex = 0;
    TransmitIndex = 0;

    /* Initialize slave address and interrupts */
    UCB0I2CSA = dev_addr;
    UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
    UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
    UCB0IE |= UCTXIE;                        // Enable TX interrupt

    UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts

    return MasterMode;

}


I2C_Mode I2C_Master_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count)
{
    /* Initialize state machine */
    MasterMode = TX_REG_ADDRESS_MODE;
    TransmitRegAddr = reg_addr;

    //Copy register data to TransmitBuffer
    CopyArray(reg_data, TransmitBuffer, count);

    TXByteCtr = count;
    RXByteCtr = 0;
    ReceiveIndex = 0;
    TransmitIndex = 0;

    /* Initialize slave address and interrupts */
    UCB0I2CSA = dev_addr;
    UCB0IFG &= ~(UCTXIFG + UCRXIFG);       // Clear any pending interrupts
    UCB0IE &= ~UCRXIE;                       // Disable RX interrupt
    UCB0IE |= UCTXIE;                        // Enable TX interrupt

    UCB0CTLW0 |= UCTR + UCTXSTT;             // I2C TX, start condition
    __bis_SR_register(LPM0_bits + GIE);              // Enter LPM0 w/ interrupts

    return MasterMode;
}

void CopyArray(uint8_t *source, uint8_t *dest, uint8_t count)
{
    uint8_t copyIndex = 0;
    for (copyIndex = 0; copyIndex < count; copyIndex++)
    {
        dest[copyIndex] = source[copyIndex];
    }
}

unsigned char CRC8(unsigned char *ptr, unsigned char len)
{
	unsigned char i;
	unsigned char crc=0;
	while(len--!=0)
	{
		for(i=0x80; i!=0; i/=2)
		{
			if((crc & 0x80) != 0)
			{
				crc *= 2;
				crc ^= 0x107;
			}
			else
				crc *= 2;

			if((*ptr & i)!=0)
				crc ^= 0x107;
		}
		ptr++;
	}
	return(crc);
}

unsigned char Checksum(unsigned char *ptr, unsigned char len)
{
	unsigned char i;
	unsigned char checksum = 0;
	
	for(i=0; i<len; i++)
		checksum += ptr[i];

	checksum = 0xff & ~checksum;

	return(checksum);
}

void I2C_WriteReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t count)
{
	#if CRC_Mode
	{
		uint8_t crc_count = 0;
		crc_count = count * 2;
		uint8_t crc1stByteBuffer [3] = {0x10, reg_addr, reg_data[0]};
		unsigned int j;
		unsigned int i;
		uint8_t temp_crc_buffer [3];
		
		TX_Buffer[0] = reg_data[0];
		TX_Buffer[1] = CRC8(crc1stByteBuffer,3);

		j = 2;
		for(i=1; i<count; i++)
		{
			TX_Buffer[j] = reg_data[i];
			j = j + 1;
			temp_crc_buffer[0] = reg_data[i];
			TX_Buffer[j] = CRC8(temp_crc_buffer,1);
			j = j + 1;
		}
		I2C_Master_WriteReg(dev_addr, reg_addr, TX_Buffer, crc_count);
	}
	#else
		I2C_Master_WriteReg(dev_addr, reg_addr, reg_data, count);
	#endif
}

int I2C_ReadReg(uint8_t dev_addr, uint8_t reg_addr, uint8_t count)
{
	unsigned int RX_CRC_Fail = 0;  // reset to 0. If in CRC Mode and CRC fails, this will be incremented.

	#if CRC_Mode
	{
		uint8_t crc_count = 0;
		crc_count = count * 2;
		unsigned int j;
		unsigned int i;
		unsigned char CRC = 0;
		uint8_t temp_crc_buffer [3];
		
		I2C_Master_ReadReg(dev_addr, reg_addr, crc_count);
		uint8_t crc1stByteBuffer [4] = {0x10, reg_addr, 0x11, ReceiveBuffer[0]};
		CRC = CRC8(crc1stByteBuffer,4);
		if (CRC != ReceiveBuffer[1])
			RX_CRC_Fail += 1;
		
		RX_Buffer[0] = ReceiveBuffer[0];
		
		j = 2; 
		for (i=1; i<count; i++)
		{
			RX_Buffer[i] = ReceiveBuffer[j];
			temp_crc_buffer[0] = ReceiveBuffer[j];
			j = j + 1;
			CRC = CRC8(temp_crc_buffer,1);
			if (CRC != ReceiveBuffer[j])
				RX_CRC_Fail += 1;
			j = j + 1;
		}
		//CopyArray(ReceiveBuffer, RX_Buffer, crc_count);
	}
	#else
		I2C_Master_ReadReg(dev_addr, reg_addr, count);
		CopyArray(ReceiveBuffer, RX_Buffer, count);
	#endif
	if (RX_CRC_Fail > 0)
		return -1;
	else
		return 0;
}

//******************************************************************************
// Device Initialization *******************************************************
//******************************************************************************


void initGPIO()
{
    //LEDs
    P1OUT = 0x00;  // Clear P1.0 output latch for defined power-on state
    P1DIR = 0x01;  // Set P1.0 to output direction
    
    P6OUT = 0x00;  // Set P6.6 to low
    P6DIR = 0x40;  // Set P6.6 to output direction
	
	// Buttons
	P2REN = 0x08;  // Set P2.3 as input. Launchpad has button to drive 0
    P2OUT = 0x08;  // on this pin. Set internal pull-up on this pin.
    P2DIR = 0x00;
    
    P4REN = 0x02;  // Set P4.1 as input
    P4OUT = 0x02;  // Set pull-up on this pin
    P4DIR = 0x00;
    
    // I2C pins
    P1SEL0 |= BIT2 | BIT3;
    P1SEL1 &= ~(BIT2 | BIT3);
	
	// Set P1.4 as interrupt from BQ76942 Alert pin
	P1IES = 0;  // P1.4 Low to High edge
	P1IE |= BIT4;   // P1.4 interrupt enabled

    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}

void initClockTo16MHz()
{
    // Configure one FRAM waitstate as required by the device datasheet for MCLK
    // operation beyond 8MHz _before_ configuring the clock system.
    FRCTL0 = FRCTLPW | NWAITS_1;

    // Clock System Setup
    __bis_SR_register(SCG0);                           // disable FLL
    CSCTL3 |= SELREF__REFOCLK;                         // Set REFO as FLL reference source
    CSCTL0 = 0;                                        // clear DCO and MOD registers
    CSCTL1 &= ~(DCORSEL_7);                            // Clear DCO frequency select bits first
    CSCTL1 |= DCORSEL_5;                               // Set DCO = 16MHz
    CSCTL2 = FLLD_0 + 487;                             // DCOCLKDIV = 16MHz
    __delay_cycles(3);
    __bic_SR_register(SCG0);                           // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));         // FLL locked
}

void initI2C()
{
    UCB0CTLW0 = UCSWRST;                      // Enable SW reset
    UCB0CTLW0 |= UCMODE_3 | UCMST | UCSSEL__SMCLK | UCSYNC; // I2C master mode, SMCLK
    UCB0BRW = 200;                            // fSCL = SMCLK/160 = ~100kHz
    UCB0I2CSA = SLAVE_ADDR;                   // Slave Address
    UCB0CTLW0 &= ~UCSWRST;                    // Clear SW reset, resume operation
    UCB0IE |= UCNACKIE;
}

void wait(volatile int x)  // waits x number of seconds
{
	volatile int i = 0;
	x *= 150;
	
	for(i = x; i>0; i--)
	{
	__delay_cycles(30000);
	}
}



//******************************************************************************
// Main ************************************************************************
// Send and receive three messages containing the example commands *************
//******************************************************************************

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    initClockTo16MHz();
    initGPIO(); 
    initI2C();
    volatile int i = 0;
    
    P1OUT = 0x00;   // Turn off red LED
    P6OUT = 0x00;	// Turn off green LED

	// This program builds on the basic example commands by illustrating a basic system example.
	// This example is using the BQ76942 for a 7-cell battery. The ALERT pin of the BQ76942 will
	// operate as an interrupt for the MCU to notify that measurements are ready or faults are 
	// present. 


	// Wait for P2.3 Button to start running program
	while (P2IN & BIT3)
        wait(1); // wait ~1 second	
	
	// RESET - returns device to default settings
	TX_2Byte[0] = 0x12; TX_2Byte[1] = 0x00;
    I2C_WriteReg(0x08, 0x3E, TX_2Byte, 2);
    wait(2);
	
	// ############# Enter CFG_UPDATE Mode to Set RAM Settings #############################
	// Enter CONFIG_UPDATE  - Command 0x0090
	TX_2Byte[0] = 0x90; TX_2Byte[1] = 0x00;
    I2C_WriteReg(0x08, 0x3E, TX_2Byte, 2);
	
	// Note: Block writing can be used to improve efficiency. In this example, write one 
	
	// Enable REG0 - 0x921B = 0x01
    TX_3Byte[0] = 0x1B; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0x01;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;  // Checksum and Length
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	

	// Enable REG1 = 3.3V - 0x921A = 0x0D
    TX_3Byte[0] = 0x1A; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0x0D;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;  // Checksum and Length
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	

	// 'VCell Mode' - Enable 7 cells - 0x92E9 = 0x023F
    TX_4Byte[0] = 0xE9; TX_4Byte[1] = 0x92; TX_4Byte[2] = 0x3F; TX_4Byte[3] = 0x02;
    I2C_WriteReg(0x08, 0x3E, TX_4Byte, 4); 
	TX_2Byte[0] = Checksum(TX_4Byte, 4); TX_2Byte[1] = 0x06;  // Checksum and Length
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);

	// Enable all protections in 'Enabled Protections A' 0x9241 = 0xFC
	TX_3Byte[0] = 0x41; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0xFC;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	

	// Enable all protections in 'Enabled Protections B' 0x9242 = 0xF7
	TX_3Byte[0] = 0x42; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0xF7;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	

	// Enable all protections in 'Enabled Protections C' 0x9243 = 0xFE
	TX_3Byte[0] = 0x43; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0xFE;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);		

	// Set TS1 to measure Cell Temperature - 0x92E2 = 0x07
	TX_3Byte[0] = 0xE2; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0x07;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	
	
	// Set TS3 to measure FET Temperature - 0x92E3 = 0x0F
	TX_3Byte[0] = 0xE4; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0x0F;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	
	
	// Set up Alert Pin - 0x92E1 = 0x2A
	TX_3Byte[0] = 0xE1; TX_3Byte[1] = 0x92; TX_3Byte[2] = 0x2A;
    I2C_WriteReg(0x08, 0x3E, TX_3Byte, 3); 
    wait(1);
	TX_2Byte[0] = Checksum(TX_3Byte, 3); TX_2Byte[1] = 0x05;
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);		
	
	// Default Alarm Mask 0x924D = 0xF882
    TX_4Byte[0] = 0x4D; TX_4Byte[1] = 0x92; TX_4Byte[2] = 0x82; TX_4Byte[3] = 0xE0;
    I2C_WriteReg(0x08, 0x3E, TX_4Byte, 4); 
	TX_2Byte[0] = Checksum(TX_4Byte, 4); TX_2Byte[1] = 0x06;  // Checksum and Length
    I2C_WriteReg(0x08, 0x60, TX_2Byte, 2);	
	

	// ############# Exit CONFIG_UPDATE Mode ###################################################
	// Exit CONFIG_UPDATE Mode - Command 0x0092
	TX_2Byte[0] = 0x92; TX_2Byte[1] = 0x00;
    I2C_WriteReg(0x08, 0x3E, TX_2Byte, 2);     	
	
	
	// FET_ENABLE command to turn on CHG and DSG FETs
	TX_2Byte[0] = 0x22; TX_2Byte[1] = 0x00;
    I2C_WriteReg(0x08, 0x3E, TX_2Byte, 2);	
    
    P1IFG &= ~BIT4; // P1.4 interrupt flag cleared
	
    while(1)
    {
		
		__bis_SR_register(LPM3_bits + GIE);  // Enter Low Power Mode 3 w/ interrupt
        __no_operation();                   // For debug
		
		// Wait for Alert pin interrupt to indicate measurements available or fault present
		
		// Read Raw Alarm Status to figure out what to do
		I2C_ReadReg(0x08, 0x62, 2);
			
		if (RX_Buffer[0] & 0x82) // If a new set of measurements is ready, go read them
		{
		    i+= 1; //Counter to flash LEDs to show measurement activity
			TX_2Byte[0] = 0x82; TX_2Byte[1] = 0x00;
			I2C_WriteReg(0x08, 0x62, TX_2Byte, 2);  // Clear Scan Alert bits
			// Read Voltage on cells
			I2C_ReadReg(0x08, 0x14, 2); // Cell 1
			I2C_ReadReg(0x08, 0x16, 2); // Cell 2
			I2C_ReadReg(0x08, 0x18, 2); // Cell 3
			I2C_ReadReg(0x08, 0x1A, 2); // Cell 4
			I2C_ReadReg(0x08, 0x1C, 2); // Cell 5
			I2C_ReadReg(0x08, 0x1E, 2); // Cell 6 (VC6)
			I2C_ReadReg(0x08, 0x26, 2); // Cell 7 (VC10)
			// Read CC2 Current
			I2C_ReadReg(0x08, 0x3A, 2);
			// Read Temperatures
			I2C_ReadReg(0x08, 0x70, 2); // Cell temp on TS1
			I2C_ReadReg(0x08, 0x74, 2); // FET temp on TS3
		}
		
		
        // Toggle the green LED every 40 measurements
        if (i > 20)
			P6OUT = 0x40;
		else
			P6OUT = 0x00;
		if (i > 40)
            i = 0;


		// Read Raw Alarm Status to figure out what to do
		I2C_ReadReg(0x08, 0x64, 2);
		
		if (RX_Buffer[1] & 0xE0) // If there is a protection triggered or PF, light up red LED
		    P1OUT = 0x01;
		else
			P1OUT = 0x00;		
	   	TX_2Byte[0] = 0x00; TX_2Byte[1] = 0xF8;
	    I2C_WriteReg(0x08, 0x62, TX_2Byte, 2);  // Try to clear Alert bits
    }
	

    __bis_SR_register(LPM0_bits + GIE);
	return 0;
}


//******************************************************************************
// I2C Interrupt ***************************************************************
//******************************************************************************
// Port 1 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) Port_1 (void)
#else
#error Compiler not supported!
#endif
{
    P1IFG &= ~BIT4;                         // Clear P1.4 IFG
    __bic_SR_register_on_exit(LPM3_bits);   // Exit LPM3
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_B0_VECTOR))) USCI_B0_ISR (void)
#else
#error Compiler not supported!
#endif
{
  //Must read from UCB0RXBUF
  uint8_t rx_val = 0;
  switch(__even_in_range(UCB0IV, USCI_I2C_UCBIT9IFG))
  {
    case USCI_NONE:          break;         // Vector 0: No interrupts
    case USCI_I2C_UCALIFG:   break;         // Vector 2: ALIFG
        __bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
        break;
    case USCI_I2C_UCNACKIFG:                // Vector 4: NACKIFG
        __bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
        break;
    case USCI_I2C_UCSTTIFG:  break;         // Vector 6: STTIFG
    case USCI_I2C_UCSTPIFG:  break;         // Vector 8: STPIFG
    case USCI_I2C_UCRXIFG3:  break;         // Vector 10: RXIFG3
    case USCI_I2C_UCTXIFG3:  break;         // Vector 12: TXIFG3
    case USCI_I2C_UCRXIFG2:  break;         // Vector 14: RXIFG2
    case USCI_I2C_UCTXIFG2:  break;         // Vector 16: TXIFG2
    case USCI_I2C_UCRXIFG1:  break;         // Vector 18: RXIFG1
    case USCI_I2C_UCTXIFG1:  break;         // Vector 20: TXIFG1
    case USCI_I2C_UCRXIFG0:                 // Vector 22: RXIFG0
        rx_val = UCB0RXBUF;
        if (RXByteCtr)
        {
          ReceiveBuffer[ReceiveIndex++] = rx_val;
          RXByteCtr--;
        }

        if (RXByteCtr == 1)
        {
          UCB0CTLW0 |= UCTXSTP;
        }
        else if (RXByteCtr == 0)
        {
          UCB0IE &= ~UCRXIE;
          MasterMode = IDLE_MODE;
          __bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
        }
        break;
    case USCI_I2C_UCTXIFG0:                 // Vector 24: TXIFG0
        switch (MasterMode)
        {
          case TX_REG_ADDRESS_MODE:
              UCB0TXBUF = TransmitRegAddr;
              if (RXByteCtr)
                  MasterMode = SWITCH_TO_RX_MODE;   // Need to start receiving now
              else
                  MasterMode = TX_DATA_MODE;        // Continue to transmision with the data in Transmit Buffer
              break;

          case SWITCH_TO_RX_MODE:
              UCB0IE |= UCRXIE;              // Enable RX interrupt
              UCB0IE &= ~UCTXIE;             // Disable TX interrupt
              UCB0CTLW0 &= ~UCTR;            // Switch to receiver
              MasterMode = RX_DATA_MODE;    // State state is to receive data
              UCB0CTLW0 |= UCTXSTT;          // Send repeated start
              if (RXByteCtr == 1)
              {
                  //Must send stop since this is the N-1 byte
                  while((UCB0CTLW0 & UCTXSTT));
                  UCB0CTLW0 |= UCTXSTP;      // Send stop condition
              }
              break;

          case TX_DATA_MODE:
              if (TXByteCtr)
              {
                  UCB0TXBUF = TransmitBuffer[TransmitIndex++];
                  TXByteCtr--;
              }
              else
              {
                  //Done with transmission
                  UCB0CTLW0 |= UCTXSTP;     // Send stop condition
                  MasterMode = IDLE_MODE;
                  UCB0IE &= ~UCTXIE;                       // disable TX interrupt
                  __bic_SR_register_on_exit(CPUOFF);      // Exit LPM0
              }
              break;

          default:
              __no_operation();
              break;
        }
        break;
    default: break;
  }
}

