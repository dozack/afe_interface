/*
 * stm32l4_system.h
 *
 *  Created on: Jul 2, 2020
 *      Author: Zoltan Dolensky
 */

#ifndef SYSTEM_INC_STM32L4_SYSTEM_H_
#define SYSTEM_INC_STM32L4_SYSTEM_H_

#include "common.h"

typedef struct {
    uint32_t hclk;
    uint32_t pclk1;
    uint32_t pclk2;
    uint32_t lseclk;
    uint32_t hseclk;
}stm32l4_system_init_t;

void stm32l4_system_peripheral_reset(uint32_t periph);
void stm32l4_system_peripheral_enable(uint32_t periph);
void stm32l4_system_peripheral_disable(uint32_t periph);
void stm32l4_system_initialize(stm32l4_system_init_t *init_struct);
void stm32l4_system_sysclk_setup(stm32l4_system_init_t *init_struct);
void stm32l4_system_lsi_control(uint8_t enable);
void stm32l4_system_lse_control(uint8_t enable);
void stm32l4_system_hsi_control(uint8_t enable);
void stm32l4_system_mco_control(uint8_t enable);
void stm32l4_system_lsco_control(uint8_t enable);
void stm32l4_system_reset(void);

#endif /* SYSTEM_INC_STM32L4_SYSTEM_H_ */
