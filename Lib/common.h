/*
 * common.h
 *
 *  Created on: Jun 28, 2020
 *      Author: Zoltan Dolensky
 */

#ifndef COMMON_H_
#define COMMON_H_

#define STM32L4
#include "stm32l4xx.h"

#ifndef NULL
#define NULL            ((void*)0)
#endif

#define LOW_BYTE(x)     (uint8_t)(x & 0xff)
#define HIGH_BYTE(x)    (uint8_t)((x >> 8) & 0xff)

#endif /* COMMON_H_ */
