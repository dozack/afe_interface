/*
 * bq769x2_common.h
 *
 *  Created on: Jul 3, 2020
 *      Author: Zoltan Dolensky
 */

#ifndef BQ769X2_BQ769X2_COMMON_H_
#define BQ769X2_BQ769X2_COMMON_H_

#include "stm32l4xx_hal.h"

#ifdef __cplusplus
#extern "C"{
#endif

/*
 * BQ769x2 COMMANDS
 *
 * 1. DIRECT_COMMAND    - target directly the address data is written to or read from
 * 2. SUBCOMMAND        - request is written to 0x3e/0x3f
 *                        response ready is checked by reading 0x3e (!= 0xff)
 *                        response data can be read (up to 32 bytes) from 0x40-0x5f
 * 3. MEMORY_COMMAND    - in case of read, same procedure as subcommands
 *                      - in case of write, checksum and length is written to 0x60/0x61 after data
 */
#define BQ_WRITE_ADDRESS                0x3e    /*!< Write address for subcommands and RAM operations */
#define BQ_READ_ADDRESS                 0x40    /*!< Read address for subcommands and RAM operations */
#define BQ_CHCKSUM_ADDRESS              0x61    /*!< Checksum register for RAM write operations */

#define BQ_WAIT_BLOCKING                0x00    /*!< Block and periodically check for data ready when reading */
#define BQ_WAIT_TIMER                   0x01    /*!< Run timer and check for data ready during interrupt routine */

#define BQ_WAIT_CONFIG                  BQ_WAIT_BLOCKING

/* I2C - Peripheral */
#define BQ_I2C_ADDRESS                  0x10
#define BQ_I2C_INSTANCE                 I2C3
#define BQ_I2C_TIMING                   0x00702991
#define BQ_I2C_ENABLE()                 RCC->APB1ENR1 |= RCC_APB1ENR1_I2C3EN
#define BQ_I2C_DISABLE()                RCC->APB1ENR1 &= ~(RCC_APB1ENR1_I2C3EN)
/* I2C - SDA pin */
#define BQ_I2C_SDA_PORT_EN()            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN
#define BQ_I2C_SDA_PORT                 GPIOC
#define BQ_I2C_SDA_PIN                  GPIO_PIN_1
#define BQ_I2C_SDA_AF                   4
/* I2C - SCL pin */
#define BQ_I2C_SCL_PORT_EN()            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN
#define BQ_I2C_SCL_PORT                 GPIOC
#define BQ_I2C_SCL_PIN                  GPIO_PIN_0
#define BQ_I2C_SCL_AF                   4
/* I2C - ALERT pin */
#define BQ_ALERT_USED                   1
#if (BQ_ALERT_USED==1)
#define BQ_ALERT_PORT_EN()              RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN
#define BQ_ALERT_PORT                   GPIOB
#define BQ_ALERT_PIN                    GPIO_PIN_0
#define BQ_ALERT_IRQN                   EXTI0_IRQn
#define BQ_ALERT_HANDLER                EXTI0_IRQHandler
#endif

/**
 * BQ769x2 interface state
 */
#define BQ_STATE_NONE                   0x00    /*!< Uninitialized state */
#define BQ_STATE_IDLE                   0x01    /*!< Interface is idle, no commands to process */
#define BQ_STATE_WAIT                   0x02    /*!< Interface is waiting for data from device */
#define BQ_STATE_BUSY                   0x03    /*!< Communication is ongoing between interface and device */

/**
 * BQ769x2 access modes
 */
#define BQ_MODE_NONE                    0x00    /*!< Interface not initialized */
#define BQ_MODE_INIT                    0x7f    /*!< Value allowing to execute commands during initialization */
#define BQ_MODE_FULLACCESS              0x01    /*!< Device in full access mode */
#define BQ_MODE_UNSEALED                0x02    /*!< Device in unsealed mode */
#define BQ_MODE_SEALED                  0x03    /*!< Device in sealed mode - safest */

/**
 * BQ769x2 command types
 */
#define BQ_CMD_NONE                     0x00    /*!< No command assigned to process */
#define BQ_CMD_DIRECT                   0x01    /*!< Direct command operation */
#define BQ_CMD_SUB                      0x02    /*!< Subcommand operation */
#define BQ_CMD_MEMORY                   0x03    /*!< Internal memory operation */

typedef struct {
        uint16_t value;
        uint8_t length;
} bq769x2_cmd_t;

/**
 * BQ769x2 interface data structure
 */
typedef struct
{
    I2C_HandleTypeDef *bus;                     /*!< TODO */
    GPIO_InitTypeDef *pins;                     /*!< TODO */
    uint8_t state;                              /*!< Actual interface state */
    uint8_t mode;                               /*!< Actual device access mode */
    uint8_t cmd_type;                           /*!< Actual command type to process */
    uint8_t alert;                              /*!< State of ALERT pin */
    uint8_t lock;                               /*!< Reserved */
    uint8_t *tx_buffer;                         /*!< Buffer for sent data */
    uint8_t tx_count;                           /*!< Number of bytes to send */
    uint8_t *rx_buffer;                         /*!< Buffer for received data */
    uint8_t rx_count;                           /*!< Number of bytes to receive */
    uint8_t *r_data;                            /*!< For read subcommands and memory operations, pointer for saving read data when available */
    uint8_t w_checksum;                         /*!< Checksum for writing volatile memory - calculated before reception */
    uint8_t w_length;                           /*!< Data length for writing volatile memory - stored before reception */
} bq769x2_device_t;

/**
 * Hardware independent procedures
 */

uint8_t bq769x2_init (bq769x2_device_t *pdevice, I2C_HandleTypeDef *i2c, GPIO_InitTypeDef *pins);

uint8_t bq769x2_poll_response (bq769x2_device_t *pdevice);

uint8_t bq769x2_write (bq769x2_device_t *pdevice, uint8_t cmd_type, uint16_t command,
                       uint8_t *pdata);

uint8_t bq769x2_read (bq769x2_device_t *pdevice, uint8_t cmd_type, uint16_t command, uint8_t *pdata);

uint8_t bq769x2_alert_status (bq769x2_device_t *pdevice);

uint8_t bq769x2_alert_reset (bq769x2_device_t *pdevice);

void bq769x2_alert_irq_handler (bq769x2_device_t *pdevice);

#ifdef __cplusplus
}
#endif

#endif /* BQ769X2_BQ769X2_COMMON_H_ */
