/*
 * bq769x2_direct.h
 *
 *  Created on: Jul 3, 2020
 *      Author: Zoltan Dolensky
 */

#ifndef BQ769X2_BQ769X2_DIRECT_H_
#define BQ769X2_BQ769X2_DIRECT_H_

#include "bq769x2_common.h"

#ifdef __cplusplus
#extern "C"{
#endif

enum bq_direct_commands_e {
    BQ_DCMD_CTL_STATUS,
    BQ_DCMD_SFT_ALERTA,
    BQ_DCMD_SFT_STATUSA,
    BQ_DCMD_SFT_ALERTB,
    BQ_DCMD_SFT_STATUSB,
    BQ_DCMD_SFT_ALERTC,
    BQ_DCMD_SFT_STATUSC,
    BQ_DCMD_PF_ALERTA,
    BQ_DCMD_PF_STATUSA,
    BQ_DCMD_PF_ALERTB,
    BQ_DCMD_PF_STATUSB,
    BQ_DCMD_PF_ALERTC,
    BQ_DCMD_PF_STATUSC,
    BQ_DCMD_PF_ALERTD,
    BQ_DCMD_PF_STATUSD,
    BQ_MEAS_VCELL1,
    BQ_MEAS_VCELL2,
    BQ_MEAS_VCELL3,
    BQ_MEAS_VCELL4,
    BQ_MEAS_VCELL5,
    BQ_MEAS_VCELL6,
    BQ_MEAS_VCELL7,
    BQ_MEAS_VCELL8,
    BQ_MEAS_VCELL9,
    BQ_MEAS_VCELL10,
    BQ_MEAS_VCELL11,
    BQ_MEAS_VCELL12,
    BQ_MEAS_VCELL13,
    BQ_MEAS_VCELL14,
    BQ_MEAS_VCELL15,
    BQ_MEAS_VCELL16,
    BQ_MEAS_VPACK,
    BQ_MEAS_VPACKPIN,
    BQ_MEAS_VLDPIN,
    BQ_VMEAS_ICC2,
    BQ_DCMD_BAT_STATUS,
    BQ_DCMD_ALRM_STATUS,
    BQ_DCMD_ALRM_RSTATUS,
    BQ_DCMD_ALRM_EN,
    BQ_MEAS_TINT,
    BQ_MEAS_TCFETOFF,
    BQ_MEAS_TDFETOFF,
    BQ_MEAS_TALERT,
    BQ_MEAS_TS1,
    BQ_MEAS_TS2,
    BQ_MEAS_TS3,
    BQ_MEAS_THDQ,
    BQ_MEAS_TDCHG,
    BQ_MEAS_DDSG,
    BQ_DCMD_FET_STATUS,
    BQ_DCMD_COUNT
};

/* Bit field definitions for direct commands - 7.5.1 */
#if 0
#define BQ_DCMD_CTL_STATUS          0x00 /* Device status bit - returns 0xffa5 when read immediately after write */
#define BQ_DCMD_SFT_ALERTA          0x02 /* Provides alert signals if enabled safety alerts are triggered */
#define BQ_DCMD_SFT_STATUSA         0x03 /* Provides fault signals if enabled safety alerts are triggered */
#define BQ_DCMD_SFT_ALERTB          0x04 /* .. */
#define BQ_DCMD_SFT_STATUSB         0x05 /* .. */
#define BQ_DCMD_SFT_ALERTC          0x06 /* .. */
#define BQ_DCMD_SFT_STATUSC         0x07 /* .. */
#define BQ_DCMD_PF_ALERTA           0x0a /* .. */
#define BQ_DCMD_PF_STATUSA          0x0b /* Provides alert signals if enabled permanent fail alerts are triggered */
#define BQ_DCMD_PF_ALERTB           0x0c /* Provides fault signals if enabled permanent fail faults are triggered */
#define BQ_DCMD_PF_STATUSB          0x0d /* .. */
#define BQ_DCMD_PF_ALERTC           0x0e /* .. */
#define BQ_DCMD_PF_STATUSC          0x0f /* .. */
#define BQ_DCMD_PF_ALERTD           0x10 /* .. */
#define BQ_DCMD_PF_STATUSD          0x11 /* .. */
#define BQ_DCMD_BAT_STATUS          0x12 /* Flags related to battery status */
#define BQ_DCMD_ALRM_STATUS         0x62 /* Latched signal to assert the ALERT pin - write 1 to clear the latch */
#define BQ_DCMD_ALRM_RSTATUS        0x64 /* Unlatched value of flags that can be selected to be latched using alarm_enable() */
#define BQ_DCMD_ALRM_EN             0x66 /* Alarm status register mask */
#define BQ_DCMD_FET_STATUS          0x7f /* Flags showing status of FETs and ALERT pin */

#define BQ_MEAS_VCELL1             0x14 /* Cell 1 voltage in millivolts */
#define BQ_MEAS_VCELL2             0x16 /* .. */
#define BQ_MEAS_VCELL3             0x18 /* .. */
#define BQ_MEAS_VCELL4             0x1a /* .. */
#define BQ_MEAS_VCELL5             0x1c /* .. */
#define BQ_MEAS_VCELL6             0x1e /* .. */
#define BQ_MEAS_VCELL7             0x20 /* .. */
#define BQ_MEAS_VCELL8             0x22 /* .. */
#define BQ_MEAS_VCELL9             0x24 /* .. */
#define BQ_MEAS_VCELL10            0x26 /* .. */
#define BQ_MEAS_VCELL11            0x28 /* .. */
#define BQ_MEAS_VCELL12            0x2a /* .. */
#define BQ_MEAS_VCELL13            0x2c /* .. */
#define BQ_MEAS_VCELL14            0x2e /* .. */
#define BQ_MEAS_VCELL15            0x30 /* .. */
#define BQ_MEAS_VCELL16            0x32 /* .. */
#define BQ_MEAS_VPACK              0x34 /* Pack voltage in userV (configurable) */
#define BQ_MEAS_VPACKPIN           0x36 /* Pack pin voltage in userV */
#define BQ_MEAS_VLDPIN             0x38 /* Load detect pin voltage in userV */

/* Coulomb counters and digital filters - 7.3.3.3 */
#define BQ_VMEAS_ICC2              0x3a /* Actual current measurement in userA (configurable) */
/* Thermistor temperature measurement - 7.3.3.8 */
#define BQ_MEAS_TINT               0x68 /* Internal temperature */
#define BQ_MEAS_TCFETOFF           0x6a
#define BQ_MEAS_TDFETOFF           0x6c
#define BQ_MEAS_TALERT             0x6e
#define BQ_MEAS_TS1                0x70 /* Termistor input 1 temperature */
#define BQ_MEAS_TS2                0x72 /* .. */
#define BQ_MEAS_TS3                0x74 /* .. */
#define BQ_MEAS_THDQ               0x76
#define BQ_MEAS_TDCHG              0x78
#define BQ_MEAS_DDSG               0x7a
#endif
/* CONTROL_STATUS */
#define BQ_CTL_STATUS_LD_ON         0x01
#define BQ_CTL_STATUS_LD_TIMEOUT    0x02
#define BQ_CTL_STATUS_DEEPSLEEP     0x04
/* SAFETY_A */
#define BQ_SFTA_CUV                 0x04
#define BQ_SFTA_COV                 0x08
#define BQ_SFTA_OCC                 0x10
#define BQ_SFTA_OCD1                0x20
#define BQ_SFTA_OCD2                0x40
#define BQ_SFTA_SCD                 0x80
/* SAFETY_B */
#define BQ_SFTB_UTC                 0x01
#define BQ_SFTB_UTD                 0x02
#define BQ_SFTB_UTINT               0x04
#define BQ_SFTB_OTC                 0x10
#define BQ_SFTB_OTD                 0x20
#define BQ_SFTB_OTINT               0x40
#define BQ_SFTB_OTF                 0x80
/* SAFETY_C */
#define BQ_SFTC_PTOS                0x08
#define BQ_SFTC_COVL                0x10
#define BQ_SFTC_OCDL                0x20
#define BQ_SFTC_SCDL                0x40
#define BQ_SFTC_OCD3                0x80
/* PF_A */
#define BQ_PFA_SUV                  0x01
#define BQ_PFA_SOV                  0x02
#define BQ_PFA_SOCC                 0x04
#define BQ_PFA_SOCD                 0x08
#define BQ_PFA_SOT                  0x10
#define BQ_PFA_SOTF                 0x40
#define BQ_PFA_CUDEP                0x80
/* PF_B */
#define BQ_PFB_CFETF                0x01
#define BQ_PFB_DFETF                0x02
#define BQ_PFB_2LVL                 0x04
#define BQ_PFB_VIMR                 0x08
#define BQ_PFB_VIMA                 0x10
#define BQ_PFB_SCDL                 0x80
/* PF_C */
#define BQ_PFC_OTPF                 0x01 /* STATUS ONLY! */
#define BQ_PFC_DRMF                 0x02 /* STATUS ONLY! */
#define BQ_PFC_IRMF                 0x04 /* STATUS ONLY! */
#define BQ_PFC_LFOF                 0x08
#define BQ_PFC_VREF                 0x10
#define BQ_PFC_VSSF                 0x20
#define BQ_PFC_HWMX                 0x40
#define BQ_PFC_CMDF                 0x80 /* STATUS ONLY! */
/* PF_D */
#define BQ_PFD_TOSF                 0x01
/* BATTERY_ STATUS bit mask */
#define BQ_BAT_CFGUPDATE            0x0001
#define BQ_BAT_PCHG_MODE            0x0002
#define BQ_BAT_SLEEP_EN             0x0004
#define BQ_BAT_POR                  0x0008
#define BQ_BAT_WD                   0x0010
#define BQ_BAT_COW_CHK              0x0020
#define BQ_BAT_OTPW                 0x0040
#define BQ_BAT_OTPB                 0x0080
#define BQ_BAT_SEC1_SEC0            0x0300
#define BQ_BAT_FUSE                 0x0400
#define BQ_BAT_SS                   0x0800
#define BQ_BAT_PF                   0x1000
#define BQ_BAT_CD_CMD               0x2000
#define BQ_BAT_SLEEP                0x8000
/* ALARM_ENABLE bit mask */
#define BQ_ALRM_WAKE                0x0001
#define BQ_ALRM_ADSCAN              0x0002
#define BQ_ALRM_CB                  0x0004
#define BQ_ALRM_FUSE                0x0008
#define BQ_ALRM_SHUTV               0x0010
#define BQ_ALRM_XDSG                0x0020
#define BQ_ALRM_XCHG                0x0040
#define BQ_ALRM_FULLSCAN            0x0080
#define BQ_ALRM_INITCOMP            0x0200
#define BQ_ALRM_INITSTART           0x0400
#define BQ_ALRM_MSK_PFALERT         0x0800
#define BQ_ALRM_MSK_SFALERT         0x1000
#define BQ_ALRM_PF                  0x2000
#define BQ_ALRM_SSA                 0x4000
#define BQ_ALRM_SSBC                0x8000
/* FET_STATUS bit mask */
#define BQ_FET_CHG_FET              0x01
#define BQ_FET_PCHG_FET             0x02
#define BQ_FET_DSG_FET              0x04
#define BQ_FET_PDSG_FET             0x08
#define BQ_FET_DCHG_PIN             0x10
#define BQ_FET_DDSG_PIN             0x20
#define BQ_FET_ALRT_PIN             0x40

#ifdef __cplusplus
}
#endif

#endif /* BQ769X2_BQ769X2_DIRECT_H_ */
