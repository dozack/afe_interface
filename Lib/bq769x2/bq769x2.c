/*
 * bq769x2_interface.c
 *
 *  Created on: Jul 1, 2020
 *      Author: zoltan
 */
#include "bq769x2_direct.h"
#include "bq769x2_sub.h"
#include "string.h"

#ifndef NULL
#define NULL            ((void*)0)
#endif

#define LOW_BYTE(x)     (uint8_t)(x & 0xff)
#define HIGH_BYTE(x)    (uint8_t)((x >> 8) & 0xff)

bq769x2_device_t *bq769x2_device = NULL;

uint8_t bq769x2_gpio_init (GPIO_InitTypeDef *pins);
uint8_t bq769x2_i2c_init (bq769x2_device_t *pdevice);
uint8_t bq769x2_i2c_write (uint8_t dev_addr, uint8_t *pdata, uint8_t count);
uint8_t bq769x2_i2c_read (uint8_t dev_addr, uint8_t target_addr, uint8_t *pdata, uint8_t count);

static const bq769x2_cmd_t direct_commands[BQ_DCMD_COUNT] = {{0x00, 2}, /* BQ_DCMD_CTL_STATUS */
                                                             {0x02, 1}, /* BQ_DCMD_SFT_ALERTA */
                                                             {0x03, 1}, /* BQ_DCMD_SFT_STATUSA */
                                                             {0x04, 1}, /* BQ_DCMD_SFT_ALERTB */
                                                             {0x05, 1}, /* BQ_DCMD_SFT_STATUSB */
                                                             {0x06, 1}, /* BQ_DCMD_SFT_ALERTC */
                                                             {0x07, 1}, /* BQ_DCMD_SFT_STATUSC */
                                                             {0x0a, 1}, /* BQ_DCMD_PF_ALERTA */
                                                             {0x0b, 1}, /* BQ_DCMD_PF_STATUSA */
                                                             {0x0c, 1}, /* BQ_DCMD_PF_ALERTB */
                                                             {0x0d, 1}, /* BQ_DCMD_PF_STATUSB */
                                                             {0x0e, 1}, /* BQ_DCMD_PF_ALERTC */
                                                             {0x0f, 1}, /* BQ_DCMD_PF_STATUSC */
                                                             {0x10, 1}, /* BQ_DCMD_PF_ALERTD */
                                                             {0x11, 1}, /* BQ_DCMD_PF_STATUSD */
                                                             {0x12, 2}, /* BQ_MEAS_VCELL1 */
                                                             {0x14, 2}, /* BQ_MEAS_VCELL2 */
                                                             {0x16, 2}, /* BQ_MEAS_VCELL3 */
                                                             {0x18, 2}, /* BQ_MEAS_VCELL4 */
                                                             {0x1a, 2}, /* BQ_MEAS_VCELL5 */
                                                             {0x1c, 2}, /* BQ_MEAS_VCELL6 */
                                                             {0x1e, 2}, /* BQ_MEAS_VCELL7 */
                                                             {0x20, 2}, /* BQ_MEAS_VCELL8 */
                                                             {0x22, 2}, /* BQ_MEAS_VCELL9 */
                                                             {0x24, 2}, /* BQ_MEAS_VCELL10 */
                                                             {0x26, 2}, /* BQ_MEAS_VCELL11 */
                                                             {0x28, 2}, /* BQ_MEAS_VCELL12 */
                                                             {0x2a, 2}, /* BQ_MEAS_VCELL13 */
                                                             {0x2c, 2}, /* BQ_MEAS_VCELL14 */
                                                             {0x2e, 2}, /* BQ_MEAS_VCELL15 */
                                                             {0x30, 2}, /* BQ_MEAS_VCELL16 */
                                                             {0x32, 2}, /* BQ_MEAS_VPACK */
                                                             {0x34, 2}, /* BQ_MEAS_VPACKPIN */
                                                             {0x36, 2}, /* BQ_MEAS_VLDPIN */
                                                             {0x38, 2}, /* BQ_VMEAS_ICC2 */
                                                             {0x3a, 2}, /* BQ_DCMD_BAT_STATUS */
                                                             {0x62, 2}, /* BQ_DCMD_ALRM_STATUS */
                                                             {0x64, 2}, /* BQ_DCMD_ALRM_RSTATUS */
                                                             {0x66, 2}, /* BQ_DCMD_ALRM_EN */
                                                             {0x68, 2}, /* BQ_MEAS_TINT */
                                                             {0x6a, 2}, /* BQ_MEAS_TCFETOFF */
                                                             {0x6c, 2}, /* BQ_MEAS_TDFETOFF */
                                                             {0x6e, 2}, /* BQ_MEAS_TALERT */
                                                             {0x70, 2}, /* BQ_MEAS_TS1 */
                                                             {0x72, 2}, /* BQ_MEAS_TS2 */
                                                             {0x74, 2}, /* BQ_MEAS_TS3 */
                                                             {0x76, 2}, /* BQ_MEAS_THDQ */
                                                             {0x78, 2}, /* BQ_MEAS_TDCHG */
                                                             {0x7a, 2}, /* BQ_MEAS_DDSG */
                                                             {0x7f, 1}, /* BQ_DCMD_FET_STATUS */
};

static const bq769x2_cmd_t subcommands[BQ_SCMD_COUNT] = {{0x0001, 6}, /* BQ_SCMD_DEVICE_NUMBER */
                                                         {0x0002, 2}, /* BQ_SCMD_FW_VERSION */
                                                         {0x0003, 2}, /* BQ_SCMD_HW_VERSION */
                                                         {0x0004, 2}, /* BQ_SCMD_IROM_SIG */
                                                         {0x0005, 2}, /* BQ_SCMD_STATIC_CFG_SIG */
                                                         {0x0007, 2}, /* BQ_SCMD_PREV_MACWRITE */
                                                         {0x0009, 2}, /* BQ_SCMD_DROM_SIG */
                                                         {0x0035, 8}, /* BQ_SCMD_SECURITY_KEYS */
                                                         {0x0053, 5}, /* BQ_SCMD_PF_STATUS */
                                                         {0x0057, 2}, /* BQ_SCMD_MANU_STATUS */
                                                         {0x0070, 32}, /* BQ_SCMD_MANU_DATA */
                                                         {0x0071, 32}, /* BQ_SCMD_DASTATUS1 */
                                                         {0x0072, 32}, /* BQ_SCMD_DASTATUS2 */
                                                         {0x0073, 32}, /* BQ_SCMD_DASTATUS3 */
                                                         {0x0074, 32}, /* BQ_SCMD_DASTATUS4 */
                                                         {0x0075, 32}, /* BQ_SCMD_DASTATUS5 */
                                                         {0x0076, 32}, /* BQ_SCMD_DASTATUS6 */
                                                         {0x0080, 32}, /* BQ_SCMD_CUV_SNAPSHOT */
                                                         {0x0081, 32}, /* BQ_SCMD_COV_SNAPSHOT */
                                                         {0x0083, 1}, /* BQ_SCMD_CB_ACTIVE_CELL */
                                                         {0x0084, 2}, /* BQ_SCMD_CB_SET_LVL */
                                                         {0x0085, 2}, /* BQ_SCMD_CBSTATUS1 */
                                                         {0x0086, 32}, /* BQ_SCMD_CBSTATUS2 */
                                                         {0x0087, 32}, /* BQ_SCMD_CBSTATUS3 */
                                                         {0x0097, 1}, /* BQ_SCMD_FET_CONTROL */
                                                         {0x0098, 1}, /* BQ_SCMD_REG12_CONTROL */
                                                         {0x00a0, 3}, /* BQ_SCMD_OTP_WR_CHECK */
                                                         {0x00a1, 3}, /* BQ_SCMD_OTP_WRITE */
                                                         {0xf081, 12}, /* BQ_SCMD_READ_CAL1 */
                                                         {0x000e, 0}, /* BQ_SCMD_EXIT_DEEPSLEEP */
                                                         {0x000f, 0}, /* BQ_SCMD_DEEPSLEEP */
                                                         {0x0010, 0}, /* BQ_SCMD_SHUTDOWN */
                                                         {0x0012, 0}, /* BQ_SCMD_RESET */
                                                         {0x001c, 0}, /* BQ_SCMD_PDSGTEST */
                                                         {0x001d, 0}, /* BQ_SCMD_FUSE_TOGGLE */
                                                         {0x001e, 0}, /* BQ_SCMD_PCHGTEST */
                                                         {0x001f, 0}, /* BQ_SCMD_CHGTEST */
                                                         {0x0020, 0}, /* BQ_SCMD_DSGTEST */
                                                         {0x0022, 0}, /* BQ_SCMD_FET_ENABLE */
                                                         {0x0024, 0}, /* BQ_SCMD_PF_ENABLE */
                                                         {0x0029, 0}, /* BQ_SCMD_PF_RESET */
                                                         {0x0030, 0}, /* BQ_SCMD_SEAL */
                                                         {0x0082, 0}, /* BQ_SCMD_RESET_PASSQ */
                                                         {0x0090, 0}, /* BQ_SCMD_SET_CFGUPDATE */
                                                         {0x0092, 0}, /* BQ_SCMD_EXIT_CFGUPDATE */
                                                         {0x0093, 0}, /* BQ_SCMD_DSG_PDSG_OFF */
                                                         {0x0094, 0}, /* BQ_SCMD_CHG_PCHG_OFF */
                                                         {0x0095, 0}, /* BQ_SCMD_ALL_FETS_OFF */
                                                         {0x0096, 0}, /* BQ_SCMD_ALL_FETS_ON */
                                                         {0x0099, 0}, /* BQ_SCMD_SLEEP_ENABLE */
                                                         {0x009a, 0}, /* BQ_SCMD_SLEEP_DISABLE */
                                                         {0x009b, 0}, /* BQ_SCMD_OCDL_RECOVER */
                                                         {0x009c, 0}, /* BQ_SCMD_SCDL_RECOVER */
                                                         {0x009d, 0}, /* BQ_SCMD_LD_RESTART */
                                                         {0x009e, 0}, /* BQ_SCMD_LD_ON */
                                                         {0x009f, 0}, /* BQ_SCMD_LD_OFF */
                                                         {0x2800, 0}, /* BQ_SCMD_CFETOFF_LO */
                                                         {0x2801, 0}, /* BQ_SCMD_DFETOFF_LO */
                                                         {0x2802, 0}, /* BQ_SCMD_ALERT_LO */
                                                         {0x2806, 0}, /* BQ_SCMD_HDQ_LO */
                                                         {0x2807, 0}, /* BQ_SCMD_DCHG_LO */
                                                         {0x2808, 0}, /* BQ_SCMD_DDSG_LO */
                                                         {0x2810, 0}, /* BQ_SCMD_CFETOFF_HI */
                                                         {0x2811, 0}, /* BQ_SCMD_DFETOFF_HI */
                                                         {0x2812, 0}, /* BQ_SCMD_ALERT_HI */
                                                         {0x2816, 0}, /* BQ_SCMD_HDQ_HI */
                                                         {0x2817, 0}, /* BQ_SCMD_DCHG_HI */
                                                         {0x2818, 0}, /* BQ_SCMD_DDSG_HI */
                                                         {0x2857, 0}, /* BQ_SCMD_PF_FORCE_A */
                                                         {0x29a3, 0}, /* BQ_SCMD_PF_FORCE_B */
                                                         {0x29bc, 0}, /* BQ_SCMD_SWAP_COMM_MODE */
                                                         {0x29e7, 0}, /* BQ_SCMD_SWAP_TO_I2C */
                                                         {0x7c35, 0}, /* BQ_SCMD_SWAP_TO_SPI */
                                                         {0x7c40, 0} /* BQ_SCMD_SWAP_TO_HDQ */
};

uint8_t _calculate_checksum (uint8_t *pdata, uint8_t length) {
    uint8_t i;
    uint8_t checksum = 0;
    for (i = 0; i < length; i++)
        checksum += pdata[i];
    checksum = 0xff & ~checksum;
    return checksum;
}

uint8_t bq769x2_init (bq769x2_device_t *pdevice, I2C_HandleTypeDef *i2c, GPIO_InitTypeDef *pins) {
    bq769x2_device = pdevice;
    pdevice->bus = i2c;
    pdevice->pins = pins;
    pdevice->mode = BQ_MODE_INIT;
    bq769x2_gpio_init(pins);
    bq769x2_i2c_init(pdevice);

//    /* Initialization of internal registers and memory blocks */
//    pobj->actl_mode = BQ_INIT;
//    /* Temporary value containing received data */
//    uint16_t value = 0;
//    /* Send reset command and wait 2 seconds */
//    bq769x2_command_write(pobj, BQ_SUBCOMMAND, BQ_SCMD_PF_RESET, 0x00, 0x00);
//    HAL_Delay(2000);
//    /* Get device number */
//    bq769x2_command_read(pobj, BQ_SUBCOMMAND, BQ_SCMD_DEVICE_NUMBER, &(pobj->device_id), 2);
//    /* Second check real actual access mode of device */
//    bq769x2_command_read(pobj, BQ_DIRECT_COMMAND, BQ_DCMD_BAT_STATUS, &value, 2);
//    uint16_t val = (value & BQ_BAT_SEC1_SEC0) >> POSITION_VAL(BQ_BAT_SEC1_SEC0);
//    pobj->actl_mode = (bq769x2_access_mode_t) val;
    pdevice->state = BQ_STATE_IDLE;
    return 1;
}

uint8_t bq769x2_poll_response (bq769x2_device_t *pdevice) {
    if (pdevice->state != BQ_STATE_WAIT)
    {
        return 0;
    }

    uint8_t res = 0;
    uint8_t tmp = 0;

    res = bq769x2_i2c_read(BQ_I2C_ADDRESS, BQ_WRITE_ADDRESS, &tmp, 1);

    if (res && tmp != 0xff && pdevice->r_data != NULL)
    {
        res = bq769x2_i2c_read(BQ_I2C_ADDRESS, BQ_READ_ADDRESS, pdevice->r_data, pdevice->rx_count);
        pdevice->state = BQ_STATE_IDLE;
    }
    return res;
}

uint8_t bq769x2_write (bq769x2_device_t *pdevice, uint8_t cmd_type, uint16_t command,
                       uint8_t *pdata) {
    if (pdevice->state != BQ_STATE_IDLE)
    {
        return 0;
    }

    uint8_t res = 0;

    pdevice->state = BQ_STATE_BUSY;
    pdevice->cmd_type = cmd_type;
    pdevice->tx_buffer = NULL;
    pdevice->tx_count = 0;
    pdevice->rx_buffer = NULL;
    pdevice->rx_count = 0;
    static uint8_t tmp_buff[34];

    switch (pdevice->cmd_type)
        {
        case BQ_CMD_DIRECT:
            tmp_buff[0] = (uint8_t) direct_commands[command].value;
            memcpy(&tmp_buff[1], pdata, direct_commands[command].length);
            pdevice->tx_buffer = tmp_buff;
            pdevice->tx_count = direct_commands[command].length + 1;
            res = bq769x2_i2c_write(BQ_I2C_ADDRESS, pdevice->tx_buffer, pdevice->tx_count);
            break;
        case BQ_CMD_SUB:
            tmp_buff[0] = BQ_WRITE_ADDRESS;
            tmp_buff[1] = LOW_BYTE(subcommands[command].value);
            tmp_buff[2] = HIGH_BYTE(subcommands[command].value);
            memcpy(&tmp_buff[3], pdata, subcommands[command].length);
            pdevice->tx_buffer = tmp_buff;
            pdevice->tx_count = subcommands[command].length + 3;
            res = bq769x2_i2c_write(BQ_I2C_ADDRESS, pdevice->tx_buffer, pdevice->tx_count);
        case BQ_CMD_MEMORY:
            pdevice->r_data = pdata;
            break;
        default:
            break;
        }
    pdevice->cmd_type = BQ_CMD_NONE;
    pdevice->state = BQ_STATE_IDLE;
    return res;
}

uint8_t bq769x2_read (bq769x2_device_t *pdevice, uint8_t cmd_type, uint16_t command, uint8_t *pdata) {
    if (pdevice->state != BQ_STATE_IDLE)
    {
        return 0;
    }

    uint8_t res = 0;

    pdevice->state = BQ_STATE_BUSY;
    pdevice->cmd_type = cmd_type;
    pdevice->tx_buffer = NULL;
    pdevice->tx_count = 0;
    pdevice->rx_buffer = pdata;
    pdevice->rx_count = 0;
    static uint8_t tmp_buff[3];

    switch (pdevice->cmd_type)
        {
        case BQ_CMD_DIRECT:
            pdevice->rx_count = direct_commands[command].length;
            res = bq769x2_i2c_read(BQ_I2C_ADDRESS, direct_commands[command].value,
                                   pdevice->rx_buffer, pdevice->rx_count);
            pdevice->state = BQ_STATE_IDLE;
            break;
        case BQ_CMD_SUB:
        case BQ_CMD_MEMORY:
            tmp_buff[0] = BQ_WRITE_ADDRESS;
            tmp_buff[1] = LOW_BYTE(subcommands[command].value);
            tmp_buff[2] = HIGH_BYTE(subcommands[command].value);
            pdevice->tx_buffer = tmp_buff;
            pdevice->tx_count = 3;
            pdevice->r_data = pdata;
            pdevice->rx_count = subcommands[command].length;
            pdevice->r_data = pdevice->rx_buffer;
            bq769x2_i2c_write(BQ_I2C_ADDRESS, pdevice->tx_buffer, pdevice->tx_count);
            pdevice->state = BQ_STATE_WAIT;
            break;
        default:
            break;
        }
    pdevice->cmd_type = BQ_CMD_NONE;
    return res;
}

uint8_t bq769x2_alert_status (bq769x2_device_t *pdevice) {
    uint8_t alert = 0;
#if(BQ_ALERT_USED == 1)
    if (pdevice != NULL)
    {
        alert = pdevice->alert;
    }
#endif
    return alert;
}

uint8_t bq769x2_alert_reset (bq769x2_device_t *pdevice) {
#if(BQ_ALERT_USED == 1)
    if (pdevice != NULL)
    {
        pdevice->alert = 0;
    }
#endif
    return 1;
}

void bq769x2_alert_irq_handler (bq769x2_device_t *pdevice) {
#if(BQ_ALERT_USED == 1)
    if (pdevice != NULL)
    {
        pdevice->alert = 1;
    }
#endif
    return;
}

/* Hardware specific functions */

uint8_t bq769x2_i2c_init (bq769x2_device_t *pdevice) {
    BQ_I2C_ENABLE();
    *(pdevice->bus) = (I2C_HandleTypeDef){0};
    pdevice->bus->Instance = BQ_I2C_INSTANCE;
    pdevice->bus->Init.Timing = 0x00702991; /* 400 kHz fast mode */
    pdevice->bus->Init.OwnAddress1 = 0;
    pdevice->bus->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    pdevice->bus->Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    pdevice->bus->Init.OwnAddress2 = 0;
    pdevice->bus->Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    pdevice->bus->Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    pdevice->bus->Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(pdevice->bus) != HAL_OK)
    {
        while (1);
        return 0;
    }
    if (HAL_I2CEx_ConfigAnalogFilter(pdevice->bus, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        while (1);
        return 0;
    }
    if (HAL_I2CEx_ConfigDigitalFilter(pdevice->bus, 0) != HAL_OK)
    {
        while (1);
        return 0;
    }
    return 1;
}

uint8_t bq769x2_gpio_init (GPIO_InitTypeDef *pins) {
    BQ_I2C_SCL_PORT_EN();
    pins->Pin = BQ_I2C_SCL_PIN;
    pins->Mode = GPIO_MODE_AF_PP;
    pins->Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    pins->Pull = GPIO_PULLUP;
    pins->Alternate = GPIO_AF4_I2C3;
    HAL_GPIO_Init(BQ_I2C_SCL_PORT, pins);
    BQ_I2C_SDA_PORT_EN();
    *pins = (GPIO_InitTypeDef) {0};
    pins->Pin = BQ_I2C_SDA_PIN;
    pins->Mode = GPIO_MODE_AF_PP;
    pins->Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    pins->Pull = GPIO_PULLUP;
    pins->Alternate = GPIO_AF4_I2C3;
    HAL_GPIO_Init(BQ_I2C_SDA_PORT, pins);
#if (BQ_ALERT_USED == 1)
    BQ_ALERT_PORT_EN();
    *pins = (GPIO_InitTypeDef) {0};
    pins->Pin = BQ_ALERT_PIN;
    pins->Mode = GPIO_MODE_IT_RISING;
    pins->Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    pins->Pull = GPIO_NOPULL;
    HAL_GPIO_Init(BQ_ALERT_PORT, pins);
    HAL_NVIC_SetPriority(BQ_ALERT_IRQN, 0, 0);
    HAL_NVIC_EnableIRQ(BQ_ALERT_IRQN);
#endif
    return 1;
}

uint8_t bq769x2_i2c_write (uint8_t dev_addr, uint8_t *pdata, uint8_t count) {
    /* TODO - get rid of those HAL libs */

    /*if (HAL_I2C_Master_Transmit(&(pobj->i2c_handle), BQ_I2C_ADDRESS, pobj->actl_cmd.tx_data,
     pobj->actl_cmd.tx_count, 50) != HAL_OK) {
     res = 0;
     }
     return res;
     */
    static uint8_t tmp[69] = {0};

    memcpy(&tmp, pdata, count);
    return 1;
}

uint8_t bq769x2_i2c_read (uint8_t dev_addr, uint8_t target_addr, uint8_t *pdata, uint8_t count) {
    /* Address accepted, read data */
    /* TODO - get rid of those HAL libs */
    uint8_t *ptr = pdata;
    static uint8_t counter = 0;
    /*
     if (HAL_I2C_Master_Receive(&(pobj->i2c_handle), BQ_I2C_ADDRESS, pobj->actl_cmd.rx_data,
     pobj->actl_cmd.rx_count, 50) != HAL_OK) {
     res = 0;
     }
     return res;
     */
    if (target_addr == BQ_WRITE_ADDRESS)
    {
        if (counter++ < 10)
        {
            *ptr = 0xff;
        }
        else
        {
            *ptr = 0x01;
            counter = 0;
        }
    }
    else if (count > 1)
    {
        *ptr++ = 0xcd;
        *ptr++ = 0xab;

    }
    else
    {
        *ptr = 0xab;
    }
    return 1;
}

void BQ_ALERT_HANDLER (void) {
    if (bq769x2_device == NULL)
    {
        return;
    }
    bq769x2_alert_irq_handler(bq769x2_device);
    NVIC_ClearPendingIRQ(BQ_ALERT_IRQN);
    EXTI->PR1 |= 1 << BQ_ALERT_PIN;
}
