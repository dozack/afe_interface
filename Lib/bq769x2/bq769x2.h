/*
 * bq769x2.h
 *
 *  Created on: Jul 5, 2020
 *      Author: Zoltan Dolensky
 */

#ifndef BQ769X2_BQ769X2_H_
#define BQ769X2_BQ769X2_H_

#include "bq769x2_common.h"
#include "bq769x2_direct.h"
#include "bq769x2_memory.h"
#include "bq769x2_sub.h"

#define BQ769X2_LIB_VERSION     1

#endif /* BQ769X2_BQ769X2_H_ */
