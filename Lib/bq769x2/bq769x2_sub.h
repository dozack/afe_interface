/*
 * bq769x2_sub.h
 *
 *  Created on: Jul 3, 2020
 *      Author: Zoltan Dolensky
 */
#include "bq769x2_common.h"

#ifndef BQ769X2_BQ769X2_SUB_H_
#define BQ769X2_BQ769X2_SUB_H_

#ifdef __cplusplus
#extern "C"{
#endif

/* Subcommands table - 7.5 */
enum bq_subcommands_e
{
    BQ_SCMD_DEVICE_NUMBER,
    BQ_SCMD_FW_VERSION,
    BQ_SCMD_HW_VERSION,
    BQ_SCMD_IROM_SIG,
    BQ_SCMD_STATIC_CFG_SIG,
    BQ_SCMD_PREV_MACWRITE,
    BQ_SCMD_DROM_SIG,
    BQ_SCMD_SECURITY_KEYS,
    BQ_SCMD_PF_STATUS,
    BQ_SCMD_MANU_STATUS,
    BQ_SCMD_MANU_DATA,
    BQ_SCMD_DASTATUS1,
    BQ_SCMD_DASTATUS2,
    BQ_SCMD_DASTATUS3,
    BQ_SCMD_DASTATUS4,
    BQ_SCMD_DASTATUS5,
    BQ_SCMD_DASTATUS6,
    BQ_SCMD_CUV_SNAPSHOT,
    BQ_SCMD_COV_SNAPSHOT,
    BQ_SCMD_CB_ACTIVE_CELL,
    BQ_SCMD_CB_SET_LVL,
    BQ_SCMD_CBSTATUS1,
    BQ_SCMD_CBSTATUS2,
    BQ_SCMD_CBSTATUS3,
    BQ_SCMD_FET_CONTROL,
    BQ_SCMD_REG12_CONTROL,
    BQ_SCMD_OTP_WR_CHECK,
    BQ_SCMD_OTP_WRITE,
    BQ_SCMD_READ_CAL1,
    BQ_SCMD_EXIT_DEEPSLEEP,
    BQ_SCMD_DEEPSLEEP,
    BQ_SCMD_SHUTDONW,
    BQ_SCMD_RESET,
    BQ_SCMD_PDSGTEST,
    BQ_SCMD_FUSE_TOGGLE,
    BQ_SCMD_PCHGTEST,
    BQ_SCMD_CHGTEST,
    BQ_SCMD_DSGTEST,
    BQ_SCMD_FET_ENABLE,
    BQ_SCMD_PF_ENABLE,
    BQ_SCMD_PF_RESET,
    BQ_SCMD_SEAL,
    BQ_SCMD_RESET_PASSQ,
    BQ_SCMD_SET_CFGUPDATE,
    BQ_SCMD_EXIT_CFGUPDATE,
    BQ_SCMD_DSG_PDSG_OFF,
    BQ_SCMD_CHG_PCHG_OFF,
    BQ_SCMD_ALL_FETS_OFF,
    BQ_SCMD_ALL_FETS_ON,
    BQ_SCMD_SLEEP_ENABLE,
    BQ_SCMD_SLEEP_DISABLE,
    BQ_SCMD_OCDL_RECOVER,
    BQ_SCMD_SCDL_RECOVER,
    BQ_SCMD_LD_RESTART,
    BQ_SCMD_LD_ON,
    BQ_SCMD_LD_OFF,
    BQ_SCMD_CFETOFF_LO,
    BQ_SCMD_DFETOFF_LO,
    BQ_SCMD_ALERT_LO,
    BQ_SCMD_HDQ_LO,
    BQ_SCMD_DCHG_LO,
    BQ_SCMD_DDSG_LO,
    BQ_SCMD_CFETOFF_HI,
    BQ_SCMD_DFETOFF_HI,
    BQ_SCMD_ALERT_HI,
    BQ_SCMD_HDQ_HI,
    BQ_SCMD_DCHG_HI,
    BQ_SCMD_DDSG_HI,
    BQ_SCMD_PF_FORCE_A,
    BQ_SCMD_PF_FORCE_B,
    BQ_SCMD_SWAP_COMM_MODE,
    BQ_SCMD_SWAP_TO_I2C,
    BQ_SCMD_SWAP_TO_SPI,
    BQ_SCMD_SWAP_TO_HDQ,
    BQ_SCMD_COUNT
};

#if 0
#define BQ_SCMD_DEVICE_NUMBER       0x0001
#define BQ_SCMD_FW_VERSION          0x0002
#define BQ_SCMD_HW_VERSION          0x0003
#define BQ_SCMD_IROM_SIG            0x0004
#define BQ_SCMD_STATIC_CFG_SIG      0x0005
#define BQ_SCMD_PREV_MACWRITE       0x0007
#define BQ_SCMD_DROM_SIG            0x0009
#define BQ_SCMD_SECURITY_KEYS       0x0035
#define BQ_SCMD_PF_STATUS           0x0053
#define BQ_SCMD_MANU_STATUS         0x0057
#define BQ_SCMD_MANU_DATA           0x0070
#define BQ_SCMD_DASTATUS1           0x0071
#define BQ_SCMD_DASTATUS2           0x0072
#define BQ_SCMD_DASTATUS3           0x0073
#define BQ_SCMD_DASTATUS4           0x0074
#define BQ_SCMD_DASTATUS5           0x0075
#define BQ_SCMD_DASTATUS6           0x0076
#define BQ_SCMD_CUV_SNAPSHOT        0x0080
#define BQ_SCMD_COV_SNAPSHOT        0x0081
#define BQ_SCMD_CB_ACTIVE_CELL      0x0083
#define BQ_SCMD_CB_SET_LVL          0x0084
#define BQ_SCMD_CBSTATUS1           0x0085
#define BQ_SCMD_CBSTATUS2           0x0086
#define BQ_SCMD_CBSTATUS3           0x0087
#define BQ_SCMD_FET_CONTROL         0x0097
#define BQ_SCMD_REG12_CONTROL       0x0098
#define BQ_SCMD_OTP_WR_CHECK        0x00a0
#define BQ_SCMD_OTP_WRITE           0x00a1
#define BQ_SCMD_READ_CAL1           0xf081
/* Command-only subcommands table */
#define BQ_SCMD_EXIT_DEEPSLEEP      0x000e
#define BQ_SCMD_DEEPSLEEP           0x000f
#define BQ_SCMD_SHUTDONW            0x0010
#define BQ_SCMD_RESET               0x0012
#define BQ_SCMD_PDSGTEST            0x001c
#define BQ_SCMD_FUSE_TOGGLE         0x001d
#define BQ_SCMD_PCHGTEST            0x001e
#define BQ_SCMD_CHGTEST             0x001f
#define BQ_SCMD_DSGTEST             0x0020
#define BQ_SCMD_FET_ENABLE          0x0022
#define BQ_SCMD_PF_ENABLE           0x0024
#define BQ_SCMD_PF_RESET            0x0029
#define BQ_SCMD_SEAL                0x0030
#define BQ_SCMD_RESET_PASSQ         0x0082
#define BQ_SCMD_SET_CFGUPDATE       0x0090
#define BQ_SCMD_EXIT_CFGUPDATE      0x0092
#define BQ_SCMD_DSG_PDSG_OFF        0x0093
#define BQ_SCMD_CHG_PCHG_OFF        0x0094
#define BQ_SCMD_ALL_FETS_OFF        0x0095
#define BQ_SCMD_ALL_FETS_ON         0x0096
#define BQ_SCMD_SLEEP_ENABLE        0x0099
#define BQ_SCMD_SLEEP_DISABLE       0x009a
#define BQ_SCMD_OCDL_RECOVER        0x009b
#define BQ_SCMD_SCDL_RECOVER        0x009c
#define BQ_SCMD_LD_RESTART          0x009d
#define BQ_SCMD_LD_ON               0x009e
#define BQ_SCMD_LD_OFF              0x009f
#define BQ_SCMD_CFETOFF_LO          0x2800
#define BQ_SCMD_DFETOFF_LO          0x2801
#define BQ_SCMD_ALERT_LO            0x2802
#define BQ_SCMD_HDQ_LO              0x2806
#define BQ_SCMD_DCHG_LO             0x2807
#define BQ_SCMD_DDSG_LO             0x2808
#define BQ_SCMD_CFETOFF_HI          0x2810
#define BQ_SCMD_DFETOFF_HI          0x2811
#define BQ_SCMD_ALERT_HI            0x2812
#define BQ_SCMD_HDQ_HI              0x2816
#define BQ_SCMD_DCHG_HI             0x2817
#define BQ_SCMD_DDSG_HI             0x2818
#define BQ_SCMD_PF_FORCE_A          0x2857
#define BQ_SCMD_PF_FORCE_B          0x29a3
#define BQ_SCMD_SWAP_COMM_MODE      0x29bc
#define BQ_SCMD_SWAP_TO_I2C         0x29e7
#define BQ_SCMD_SWAP_TO_SPI         0x7c35
#define BQ_SCMD_SWAP_TO_HDQ         0x7c40
#endif
/* Bit field definitions for subcommands */
#define BQ_MANU_STATUS_PCHG_TEST    0x0001
#define BQ_MANU_STATUS_CHG_TEST     0x0002
#define BQ_MANU_STATUS_DSG_TEST     0x0004
#define BQ_MANU_STATUS_FET_EN       0x0010
#define BQ_MANU_STATUS_PDSG_TEST    0x0020
#define BQ_MANU_STATUS_PF_EN        0x0040
#define BQ_MANU_STATUS_OTPW_EN      0x0080
#define BQ_FET_CONTROL_DSG_OFF      0x0001
#define BQ_FET_CONTROL_PDSG_OFF     0x0002
#define BQ_FET_CONTROL_CHG_OFF      0x0004
#define BQ_FET_CONTROL_PCHG_OFF     0x0008
#define BQ_REG12_CONTROL_REG1_EN    0x0001
#define BQ_REG12_CONTROL_REG1V_MSK  0x000e
#define BQ_REG12_CONTROL_REG2_EN    0x0010
#define BQ_REG12_CONTROL_REG2V_MSK  0x00e0
/* Values for both REG1V and REG2V registers */
/* Example: (BQ_REG12_CONTROL_REGV_1_8 << POSITION_VAL(BQ_REG12_CONTROL_REG2V_MSK) */
#define BQ_REG12_CONTROL_REGV_1_8   0x0003
#define BQ_REG12_CONTROL_REGV_2_5   0x0004
#define BQ_REG12_CONTROL_REGV_3_0   0x0005
#define BQ_REG12_CONTROL_REGV_3_3   0x0006
#define BQ_REG12_CONTROL_REGV_5_0   0x0007

#ifdef __cplusplus
}
#endif

#endif /* BQ769X2_BQ769X2_SUB_H_ */
